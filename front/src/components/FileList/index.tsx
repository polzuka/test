import React, {useRef, useState} from 'react';
import {AxiosError} from "axios";
import cx from 'classnames';
import styles from './styles.module.css'
import Button from "../Button";
import {useMutation, useQuery} from "react-query";
import api, {IDeleteFileParams, IFileListResponse, IUploadFileParams} from "../../api";
import Spinner from "../Spinner";
import Error from '../Error';
import File from '../File';

interface IFileList {
    multiple: boolean
    title: string
    name: string
}

const FileField = ({multiple, title, name: fieldName}: IFileList) => {
    const fileInputRef = useRef<HTMLInputElement>(null);
    const [fileNames, setFiles] = useState<string[]>([]);
    const [error, setError] = useState<string|null>(null);

    const onLoadFileList = (data: IFileListResponse) => {
        setFiles(data.files || []);
        setError(data.error || null);
    };

    const onUploadFile = (data: IFileListResponse) => {
        setFiles(data.files || []);
        setError(data.error || null);
    };

    const onError = () => {
        setError('Ошибка');
    };

    const {isLoading: isFileListLoading} = useQuery<IFileListResponse, AxiosError>(
        ['files', fieldName],
        () => api.getFileList({fieldName}),
        {
            onSuccess: onLoadFileList,
            onError,
        }
    );

    const {mutate: uploadFile, isLoading: isUploadingFile}
        = useMutation<IFileListResponse, AxiosError, IUploadFileParams>(api.uploadFile, {
            onSuccess: onUploadFile,
            onError,
        });

    const {mutate: deleteFile, isLoading: isDeletingFile}
        = useMutation<IFileListResponse, AxiosError, IDeleteFileParams>(api.deleteFile, {
            onSuccess: onLoadFileList,
            onError,
        });

    const addFile = async (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files === null) {
            return;
        }

        const filteredFiles = Array.from(event.target.files).filter((file) => {
            return !fileNames.includes(file.name);
        });

        if (filteredFiles.length === 0) {
            return;
        }

        if (!multiple && fileNames.length > 0) {
            await deleteFile({fieldName, fileName: fileNames[0]});
        }

        await uploadFile({fieldName, files: filteredFiles});
    };

    const openDialog = () => {
        if (fileInputRef.current === null) {
            return;
        }

        fileInputRef.current.click();
    };

    return (
        <div className={cx(styles.component)}>
            <label>{title}</label>

            <div className={styles.wrapper}>
                <div className={styles.files}>
                    {fileNames.map((fileName, index) => (
                        <File
                            key={fileName}
                            fieldName={fieldName}
                            fileName={fileName}
                            onCancel={() => deleteFile({fieldName, fileName})}
                        />
                    ))}
                </div>

                <input
                    ref={fileInputRef}
                    type="file"
                    name={fieldName}
                    onChange={addFile}
                    className={styles.input}
                    multiple={multiple}
                />

                <Button
                    icon='upload'
                    onClick={openDialog}
                    disabled={isUploadingFile}
                />

                {isFileListLoading || isDeletingFile || isUploadingFile && <Spinner/>}
            </div>

            {error && <Error>{error}</Error>}
        </div>
    )
};

export default FileField
