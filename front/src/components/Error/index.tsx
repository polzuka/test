import React from 'react';
import styles from './styles.module.css'

interface IError {
    readonly children: string
}

const Error = ({children}: IError) => {
    return (
        <div className={styles.error}>{children}</div>
    )
}

export default Error
