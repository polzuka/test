import React from 'react';
import styles from './styles.module.css'
import Spinner from "../Spinner";
import Button from "../Button";

interface IFile {
    fieldName: string
    fileName: string
    onCancel: () => void
    uploaded?: boolean
}

const File = ({fieldName, fileName, onCancel, uploaded = false}: IFile) => {
    return (
        <div className={styles.file}>
            {uploaded ? <Spinner/> : <div className={styles.placeholder}/>}
            <a
                className={styles.link}
                href={`/api/storage/${fieldName}/${fileName}`}
                target="_blank"
            >{fileName}</a>
            <Button
                onClick={() => onCancel()}
                icon="remove"
            />
        </div>
    )
}

export default File
