import React from 'react';
import cx from 'classnames';
import styles from './styles.module.css'

interface ISpinner {
    className?: string;
}

const Spinner = ({className}: ISpinner) =>  <div className={cx(styles.spinner, className)}/>;

export default Spinner
