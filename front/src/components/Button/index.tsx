import React from 'react';
import cx from 'classnames';
import styles from './styles.module.css'

interface IButton {
    text?: string
    icon?: string
    onClick?: () => void
    disabled?: Boolean
}

const Button = ({
    text,
    icon,
    onClick,
    disabled,
}: IButton) => {
    return (
        <div onClick={onClick} className={cx(styles.button, {[styles.disabled]: disabled})}>
            {text && <span>{text}</span>}
            {icon && <div className={cx(styles.icon, styles[icon])}/>}
        </div>
    )
};

export default Button
