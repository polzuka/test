import React from 'react';
import styles from './styles.module.css'

interface IBadge {
    readonly children: string
}

const Badge = ({children}: IBadge) => {
    return (
        <span className={styles.badge}>{children}</span>
    )
}

export default Badge
