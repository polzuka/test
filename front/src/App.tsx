import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import Main from './pages/Main';
import {QueryClientProvider} from "react-query";
import { QueryClient } from 'react-query'
import styles from './styles.module.css'
const queryClientConfig = {
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
};

const queryClient = new QueryClient(queryClientConfig);

function App() {
  return (
      <div className={styles.app}>
          <QueryClientProvider client={queryClient}>
              <Router>
                  <Switch>
                      <Route path="/"><Main/></Route>
                  </Switch>
              </Router>
          </QueryClientProvider>
      </div>
  );
}

export default App;
