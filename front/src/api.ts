import axios from "axios";

export interface IUploadFileParams {
    fieldName: string
    files: Blob[]
}

export interface IDeleteFileParams {
    fieldName: string
    fileName: string
}

export interface IFetchFileParams {
    fieldName: string
}

export interface IFileListResponse {
    error?: string
    files: string[]
}

const uploadFile = ({fieldName, files}: IUploadFileParams) => {
    const data = new FormData();
    for (const file of Array.from(files)) {
        data.append('files', file);
    }

    return axios.post<IFileListResponse>(`/api/files/${fieldName}`, data)
        .then(({data}) => data)
};

const getFileList = ({fieldName}: IFetchFileParams) => {
    return axios.get<IFileListResponse>(`/api/files/${fieldName}`)
        .then(({data}) => data)
};

const deleteFile = ({fieldName, fileName}: IDeleteFileParams) => {
    return axios.delete<IFileListResponse>(`/api/files/${fieldName}`, {data: {fileName}})
        .then(({data}) => data)
};

const api = {
    uploadFile,
    getFileList,
    deleteFile,
};

export default api
