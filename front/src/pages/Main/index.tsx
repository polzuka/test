import React from 'react';
import styles from './styles.module.css';
import FileField from "../../components/FileList";

const Main = () => {
    return (
        <div className={styles.container}>
            <FileField
                name="multiple"
                multiple={true}
                title="Множественное поле"
            />

            <FileField
                name="single"
                multiple={false}
                title="Одиночное поле"
            />
        </div>
    );
};

export default Main;
