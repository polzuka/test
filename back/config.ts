interface IConfig {
    filesPath: string
    nodeEnv: string
}

const config: IConfig = {
    filesPath: process.env.FILES_PATH!,
    nodeEnv: process.env.NODE_ENV!,
};

export default config;
