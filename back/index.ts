import express from 'express';
import cors from 'cors';
import apiRouter from './apiRouter';
import {mkdirSync} from "fs";
import config from './config';

process.on('unhandledRejection', error => {
    console.error('unhandledRejection', error);
});

process.on('uncaughtException', error => {
    console.error('uncaughtException', error);
});

mkdirSync(config.filesPath, {recursive: true});

const app = express();
const PORT = 3001;

if (config.nodeEnv !== 'production') {
    app.use(cors());
}

app.use('/api', apiRouter);

app.listen(PORT, () => {
    console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`);
});
