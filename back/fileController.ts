import {Request, Response} from "express";
import {UploadedFile} from "express-fileupload";
import {promises as fsp} from 'fs';
import { join } from "path";
import config from "./config";

const getFiles = async (fieldName: string) => {
    const path = join(config.filesPath, fieldName);
    try {
        await fsp.mkdir(path);
    } catch (error) {
        if (error.code !== 'EEXIST') {
            throw error;
        }
    }
    return fsp.readdir(path);
};

const getFilePath = (fieldName: string, fileName: string) => {
    return join(config.filesPath, fieldName, fileName);
};

const promisifyFileMv = (fieldName: string, file: UploadedFile) => {
    return new Promise((resolve, reject) => {
        file.mv(getFilePath(fieldName, file.name), async (error) => {
            return error ? reject(error) : resolve(file.name);
        });
    });
};

export const uploadFile = async (req: Request, res: Response) => {
    if (!req.files) {
        return res.json({
            files: [],
        });
    }

    const fieldName = req.params.fieldName;
    const files = await getFiles(fieldName);


    const uploadedFiles: UploadedFile[] = Array.isArray(req.files.files)
        ? req.files.files
        : [req.files.files];

    const newFiles = await Promise.all(uploadedFiles.map((file) => promisifyFileMv(fieldName, file)));

    const updatedFiles = [...files, ...newFiles];
    return res.json({files: updatedFiles});
};

export const deleteFile = async (req: Request, res: Response) => {
    await fsp.rm(getFilePath(req.params.fieldName, req.body.fileName), {force: true});
    const files = await getFiles(req.params.fieldName);
    return res.json({files});
};

export const getFileList = async (req: Request, res: Response) => {
    const files = await getFiles(req.params.fieldName);
    return res.json({files});
};
