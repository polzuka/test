import express from "express";
import {deleteFile, getFileList, uploadFile} from "./fileController";
import fileUpload from "express-fileupload";
import bodyParser from 'body-parser';
import {handleError} from "./handleError";
import config from "./config";

const fileUploadMiddleware = fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
});

const router = express.Router();

router.use('/storage', express.static(config.filesPath));
router.get('/files/:fieldName', handleError(getFileList));
router.post('/files/:fieldName', fileUploadMiddleware, handleError(uploadFile));
router.delete('/files/:fieldName', bodyParser.json(), handleError(deleteFile));

export default router;
