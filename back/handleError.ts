import {Request, Response} from "express";

export const handleError = (fn: (req: Request, res: Response) => void) => async (req: Request, res: Response) => {
    try {
        await fn(req, res);
    }
    catch (error) {
        console.error(error);
        res.status(500).json({error: 'Ошибка'});
    }
};
