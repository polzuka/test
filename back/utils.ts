import {v4 as uuidv4} from "uuid";
import ffmpeg, {FfmpegCommand} from 'fluent-ffmpeg';

export const getUid = () => {
    return process.env.NODE_ENV !== 'production' ? '0' : uuidv4();
};

export const getPromiseFromEmitter = (start: (command: FfmpegCommand) => void) => {
    const command = ffmpeg();
    return new Promise(
        (resolve, reject) => {
            command.on('end', resolve);
            command.on('error', reject);
            command.on('start', (commandLine: string) => console.log(commandLine));
            start(command)
        }
    );
};

export const getPromiseFromCallback = (start: (command: FfmpegCommand, resolve: Function, reject: Function) => void) => {
    const command = ffmpeg();
    return new Promise(
        (resolve, reject) => {
            start(command, resolve, reject)
        }
    );
};
